﻿route_id,agency_id,route_short_name,route_long_name,route_type,route_color,route_text_color
"3-1-mjp-1","","1","East Coburg - South Melbourne Beach","0","78BE20","FFFFFF"
"3-109-mjp-1","","109","Port Melbourne - Box Hill","0","78BE20","FFFFFF"
"3-11-mjp-1","","11","West Preston - Victoria Harbour Docklands","0","78BE20","FFFFFF"
"3-12-mjp-1","","12","St Kilda (Fitzroy St) - Victoria Gardens","0","78BE20","FFFFFF"
"3-16-mjp-1","","16","Kew - Melbourne University via St Kilda Beach","0","78BE20","FFFFFF"
"3-19-mjp-1","","19","North Coburg - Flinders Street Station (City)","0","78BE20","FFFFFF"
"3-3-mjp-1","","3/3a","Melbourne University - East Malvern","0","78BE20","FFFFFF"
"3-30-mjp-1","","30","Etihad Stadium Docklands - St Vincents Plaza","0","78BE20","FFFFFF"
"3-35-mjp-1","","35","Clockwise - Clockwise","0","78BE20","FFFFFF"
"3-48-mjp-1","","48","North Balwyn - Victoria Harbour Docklands","0","78BE20","FFFFFF"
"3-5-mjp-1","","5","Melbourne University - Malvern (Burke Road)","0","78BE20","FFFFFF"
"3-57-mjp-1","","57","West Maribyrnong - Flinders Street Station (City)","0","78BE20","FFFFFF"
"3-58-mjp-1","","58","Toorak - West Coburg","0","78BE20","FFFFFF"
"3-59-mjp-1","","59","Airport West - Flinders Street Station (City)","0","78BE20","FFFFFF"
"3-6-mjp-1","","6","Glen Iris - Moreland","0","78BE20","FFFFFF"
"3-64-mjp-1","","64","Melbourne University - East Brighton","0","78BE20","FFFFFF"
"3-67-mjp-1","","67","Carnegie - Melbourne University","0","78BE20","FFFFFF"
"3-70-mjp-1","","70","Waterfront City Docklands - Wattle Park","0","78BE20","FFFFFF"
"3-72-mjp-1","","72","Melbourne University - Camberwell","0","78BE20","FFFFFF"
"3-75-mjp-1","","75","Vermont South - Etihad Stadium Docklands","0","78BE20","FFFFFF"
"3-78-mjp-1","","78","Balaclava via Prahran - North Richmond","0","78BE20","FFFFFF"
"3-82-mjp-1","","82","Footscray - Moonee Ponds","0","78BE20","FFFFFF"
"3-86-mjp-1","","86","Bundoora RMIT - Waterfront City Docklands","0","78BE20","FFFFFF"
"3-96-mjp-1","","96","St Kilda Beach - East Brunswick","0","78BE20","FFFFFF"
